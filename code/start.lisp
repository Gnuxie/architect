#|
    Copyright (C) 2019-2020 Gnuxie <Gnuxie@protonmail.com>
|#

;(cl-mongo:pp (cl-mongo:iter (cl-mongo:db.find "ARCHITECT.MODELS.ROOM-SEND::MOCK-PDU" :all)))

(defun start ()
  (setf blackbird:*debug-on-error* t)
  (setf (verbose:repl-level) :debug)
  (bt:make-thread (architect.wookie:make-server (make-instance 'wookie:listener :bind nil :port 4003) :wookie-debug t :blackbird-debug t))
  (architect.models.room-send:add-room-worker)
  (asdf:load-system :cl-matrix)
  (defvar *account* (cl-matrix:make-account "@foo:localhost" ""
                                            :origin "http://localhost:4003"))
  (cl-mongo:db.use "test"))

