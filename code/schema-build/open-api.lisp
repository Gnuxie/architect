#|
    Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:architect.schema-build)

(defvar *json-schemas*
  (mapcar
   (lambda (s)
     (asdf:system-relative-pathname '#:architect s))
   '("matrix-doc/event-schemas/schema/m.room.message"
     "matrix-doc/event-schemas/schema/core-event-schema/state_event.yaml"
     "matrix-doc/api/server-server/definitions/pdu_v1.yaml"
     "matrix-doc/event-schemas/schema/m.room.create"
     "matrix-doc/event-schemas/schema/m.room.member"
     "matrix-doc/event-schemas/schema/m.room.power_levels"
     "matrix-doc/event-schemas/schema/m.room.join_rules"
     "matrix-doc/event-schemas/schema/m.room.history_visibility")))


