#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:architect.schema.options
  (:use #:cl)
  (:export
   #:matrix-option
   #:matrix-content-class
   #:matrix-content
   #:event-content-p
   #:event-type<-schema
   #:event-type))

(in-package #:architect.schema.options)

(defclass matrix-option (json-schema:mop-option)
  ())

(defclass matrix-content-class (json-schema:json-serializable-class)
  ((%event-type :reader event-type
                :type string
                :documentation "The event type given to a pdu if it is supplied
with an instance of a content class that is an instance of this metaclass.")))

(defmethod shared-initialize :before ((class matrix-content-class) slot-names
                                      &key event-type &allow-other-keys)
  (declare (ignore slot-names))
  (if event-type
      (setf (slot-value class '%event-type)
            (car event-type))
      (slot-makunbound class '%event-type)))

(defun event-content-p (schema)
  (declare (type json-schema.schema:inner-schema schema))
  (and (not (typep (json-schema.schema:parent schema)
                   'json-schema.schema:inner-schema))
       (string= "content" (json-schema.schema:parent-key schema))))

(defun event-type<-schema (schema)
  (declare (type json-schema.schema:inner-schema schema))
  (let ((type (car (json-schema.schema:hash-filter
                    (json-schema.schema:object (json-schema.schema:parent schema))
                    "properties" "type" "enum"))))
    (unless type
      (error "Missing type enum on schema ~a, add with properties:~%
  type:~%  enum:~%  - my.event.type" (json-schema.schema:name
                                      (json-schema.schema:parent schema))))
    type))

(defmethod json-schema:class-options<-schema ((schema json-schema.schema:inner-schema)
                                              (option matrix-option))
  (if (event-content-p schema)
      `((:metaclass matrix-content-class)
        (:event-type ,(event-type<-schema schema)))
      (call-next-method)))

(defmethod c2mop:validate-superclass ((class matrix-content-class)
                                      (super json-schema:json-serializable-class))
  t)

(defclass matrix-content (json-schema:json-serializable)
  ())

(defmethod event-type ((instance matrix-content))
  (event-type (class-of instance)))

(defmethod c2mop:class-direct-superclasses ((class matrix-content-class))
  (append (remove (find-class 'json-schema:json-serializable)
                  (remove (find-class 'standard-object) (call-next-method)))
          (list (find-class 'matrix-content)
                (find-class 'json-schema:json-serializable)
                (find-class 'standard-object))))
