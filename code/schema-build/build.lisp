#|
    Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:architect.schema-build)

(defun create-json-schema (uri option)
  (let ((schema (json-schema.schema:find-schema uri)))
    (json-schema:ensure-schema-class schema option)))

(defun create-json-schemas (schemas)
  (let ((option
         (make-instance 'architect.schema.options:matrix-option
                        :whitelist '("room_event" "state_event" "pdu_v1")
                        :package-designator "ARCHITECT.SCHEMA")))
    (mapc (lambda (s)
            (create-json-schema s option))
          schemas)))

(defpackage-plus-1:ensure-package '#:architect.schema)
(create-json-schemas *json-schemas*)
