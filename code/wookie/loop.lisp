#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:architect.wookie
  (:use #:cl)
  (:export
   #:attach-response-handler
   #:make-server
   #:with-wookie-loop))

(in-package #:architect.wookie)

(defvar *loop-mailbox*)
(defvar *empty-mailbox-notifier*)

(defun attach-response-handler (promise-gen response)
  (bb:attach
   promise-gen
   (let ((loop-mailbox *loop-mailbox*)
         (loop-notifier *empty-mailbox-notifier*))
     (lambda (result)
       (architect.hacks.async:defer-task loop-mailbox
           (lambda () (wookie:send-response response :body result)))
       (as:trigger-notifier loop-notifier)))))

(defun report-api-error (response c)
  (wookie:send-response
   response
   :body (format nil "{\"errcode\":\"M_UKNOWN\",\"errmsg\":\"~a\"}" c)
   :status 500))

(defmacro with-wookie-loop ((promise-gen response result-var) &body body)
  `(let ((loop-mailbox *loop-mailbox*)
         (loop-notifier *empty-mailbox-notifier*))
     (bb:catcher (bb:attach
                  ,promise-gen
                  (lambda (,result-var)
                    (architect.hacks.async:defer-task loop-mailbox
                        (lambda () ,@body))
                    (as:trigger-notifier loop-notifier)))
                 (error (c)
                   (architect.hacks.async:defer-task loop-mailbox
                       (lambda ()
                         (report-api-error ,response c)))
                   (as:trigger-notifier loop-notifier)))))

;;; event loop doesn't run the body each time, it's just an init.
;;; if tehre are no events at the end it dies.
(defun make-server (listener &key wookie-debug blackbird-debug)
  (lambda ()
    (let ((wookie-config:*debug-on-error* wookie-debug)
          (blackbird:*debug-on-error* blackbird-debug))
      (as:with-event-loop ()
        (setf *loop-mailbox* (safe-queue:make-mailbox))
        (setf *empty-mailbox-notifier*
              (as:make-notifier
               (lambda ()
                 (as:delay
                  (lambda ()
                    (loop :until (safe-queue:mailbox-empty-p *loop-mailbox*)
                       :do (architect.hacks.async:execute-message
                            (safe-queue:mailbox-receive-message-no-hang *loop-mailbox*))))))
               :single-shot nil))
        (v:info :event-loop "Starting server")
        (let ((server (wookie:start-server listener)))
          (declare (ignorable server)))))))
