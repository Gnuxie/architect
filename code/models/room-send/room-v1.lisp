#|
    Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:architect.models.room-send)

(defclass room-v1 (matrix-room)
  ())

(defmethod authorize ((matrix-room room-v1) event)
  (cond
    ((string= "m.room.create" (architect.schema:event-type event))
     (error "fuck."))

    ))

(defmethod reference-hash ((room room-v1) object)
  (let* ((alist
          (remove-if (lambda (thing)
                       (member (car thing) '("unsigned" "signature" "hashes")
                               :test #'string=))
                     (architect.hacks.canonical-json:alist<-json object)))
         (canonical-json (babel:string-to-octets
                          (architect.hacks.canonical-json:canonical-json alist))))
    (qbase64:encode-bytes
     (ironclad:digest-sequence (ironclad:make-digest :sha256) canonical-json))))
