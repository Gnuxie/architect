#|
    Copyright (C) 2019-2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:architect.models.room-send)

(defvar *room-shelf*)
(defvar *room-shelves*
  (list)
  "list of weak pointers pointing to all active room-shelves.

for monitoring purposes only.")

(defun make-room-shelf ()
  (let ((shelf (cache:make-cache-table :test #'equal :size 256)))
    (push (trivial-garbage:make-weak-pointer shelf) *room-shelves*)
    shelf))

(defun retrieve-room-from-store (room-id)
  ;; yes this needs actually impl
  (make-instance 'mock-room)
  )

(defun get-room (room-id)
  "Return the a object

finds the room from the store if it isn't present and puts it into the shelf.


needs changing because the room-id might be invlid for retrieve-room-from-store

maybe we will just use non local exit for that anyways."
  (cache:cache-table-get-or-fill room-id *room-shelf*
                                 #'retrieve-room-from-store
                                 :expire 1800))

;;; this needs a setf that can be used from create room which is why message
;;; send does not work yet.
