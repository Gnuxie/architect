#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:architect.models.room-send)

(defclass mock-room (matrix-room)
  ()
  (:metaclass cl-mongo-meta:collection-class))

(defclass mock-pdu (architect.schema:pdu-v1)
  ((%split :initarg :split
           :accessor split
           :type rational
           :initform 1/1
           :documentation "The ratio of the split that the branch holds.
This concept is adapted from joepie91 who's homeserver can be found here: https://git.cryto.net/joepie91/new-home/ "
           :json-ghost t
           :key-name "split")
   )
  (:metaclass json-schema.cl-mongo:json/collection-class))

(defmethod base-pdu<-content ((room mock-room) &rest initargs)
  (let ((pdu (apply #'make-instance 'mock-pdu
                    initargs)))
    ;; honestly, can't find a lib to do this properly
    ;; unix time is in seconds not miliseconds
    ;; so no one in CL has ever needed to make one for ms
    ;; since the standard has one in s.
    (setf (architect.schema:origin-server-ts pdu)
          (* (get-universal-time) 1000))

    ;; need to sort out the config for server name.
    (setf (architect.schema:origin pdu)
          "CHANGEME")

    (setf (architect.schema:sender pdu)
          "SHOULDNOTBEINITALISEDHERE@hardcoded.net")

    (setf (architect.schema:prev-events pdu)
          (previous-events room))

    (setf (architect.schema:auth-events pdu)
          (select-auth-events room pdu))

    ;; need to make sure the incf of depth is not preserved outiside the transaction.
    (setf (architect.schema:depth pdu)
          (detph room))

    ;; content hash, reference hash, and signing can all be done in one place
    ;; using the sorted alist representation of the pdu.
    (setf (architect.schema:hashes pdu)
          (make-instance 'architect.schema:pdu-v1-hashes
                         :sha256 (content-hash room pdu)))
    pdu))

(defmethod update-room ((room mock-room) (pdu architect.schema:pdu-v1) &key)
  (incf (detph room))
  (setf (previous-events room)
        (list (list (architect.schema:event-id pdu)
                    ;; hack, the reference hash should definitly be stored in hashes.
                    (subseq (architect.schema:event-id pdu)
                            1))))
  (when (slot-boundp pdu 'architect.schema:state-key)
    (setf (getstate room (architect.schema:type pdu) (architect.schema:state-key pdu))
          pdu))
  room)

(defmethod content-hash ((room mock-room) (pdu architect.schema:pdu-v1))
  (qbase64:encode-bytes
   (ironclad:digest-sequence
    (ironclad:make-digest :sha256)
    (babel:string-to-octets
     (architect.hacks.canonical-json:canonical-json pdu "unsigned" "signatures" "hashes")))))

(defmethod reference-hash ((room mock-room) (pdu architect.schema:pdu-v1))
  (qbase64:encode-bytes
   (ironclad:digest-sequence
    (ironclad:make-digest :sha256)
    (babel:string-to-octets
     (architect.hacks.canonical-json:canonical-json
      (redact-pdu pdu) "unsigned" "age_ts" "signatures")))))

(defmethod complete-pdu<-base-pdu ((room mock-room)
                                   (base-pdu architect.schema:pdu-v1) &key)

  (setf (architect.schema:unsigned base-pdu)
        (make-instance 'architect.schema:unsigned-pdu-base-unsigned
                       :reference-hash (reference-hash room base-pdu)))

  (setf (architect.schema:event-id base-pdu)
        (format nil "$~a" (architect.schema:reference-hash
                           (architect.schema:unsigned base-pdu))))

  base-pdu)

(defmethod authorize ((room mock-room) (pdu architect.schema:pdu-v1))
  pdu)

(defmethod store-new-pdu ((room mock-room) (pdu architect.schema:pdu-v1))
  (cl-mongo-meta:db.insert pdu))

(defmethod add-from-content ((room mock-room) content &rest args)
  (let ((pdu (complete-pdu<-base-pdu
              room (apply #'base-pdu<-content room :content content args))))
    (authorize room pdu)
    (store-new-pdu room pdu)
    (update-room room pdu)
    pdu))

(defmethod add-from-content ((room mock-room)
                             (content architect.schema.options:matrix-content)
                             &rest args)
  (apply #'call-next-method room content
         (list*
          :type (architect.schema.options:event-type content)
          args)))

(defmethod getstate ((room mock-room) (state-type string) &optional (state-key ""))
  (find state-key
        (cdr (assoc state-type (%state room) :test #'string=))
        :key #'architect.schema:state-key
        :test #'string=))

(defmethod (setf getstate) (new-state (room mock-room) (state-type string)
                            &optional (state-key ""))
;;; smh, remove with (values new-sequcne items) would be nice.
  (let ((old-state nil))
    (let* ((association (assoc state-type (%state room) :test #'string=))
           (clean-state
            (delete-if (lambda (event)
                         (when (string= state-key
                                        (architect.schema:state-key event))
                           (setf old-state event)
                           t))
                       (cdr association))))

      (if association
        (setf (cdr association)
              (push new-state
                    clean-state))
        (setf (%state room)
              (acons state-type (list new-state) (%state room)))))))

(defmethod select-auth-events ((room mock-room) (pdu architect.schema:pdu-v1))
  "https://matrix.org/docs/spec/server_server/unstable#auth-events-selection

missing third-party invites."
  (if (string= "m.room.create" (architect.schema:type pdu))
      '()
      (let ((auth-events '()))
        (flet ((add-auth (pdu)
                 (push (list (architect.schema:event-id pdu)
                             (architect.schema:reference-hash
                              (architect.schema:unsigned pdu)))
                       auth-events)))
          (add-auth (getstate room "m.room.create"))
          (anaphora:awhen (getstate room "m.room.power_levels")
            (add-auth anaphora:it))
          (anaphora:awhen (getstate room "m.room.member"
                                    (architect.schema:sender pdu))
            (add-auth anaphora:it))

          (when (string= "m.room.member" (architect.schema:type pdu)) 
            (anaphora:awhen (getstate room "m.room.member"
                                      (architect.schema:state-key pdu))
              (add-auth anaphora:it))
            (let ((membership (architect.schema:membership
                               (architect.schema:content pdu))))
              (cond ((or (string= "join" membership) (string= "invite" membership))
                     (anaphora:awhen (getstate room "m.room.join_rules")
                       (add-auth anaphora:it)))))))
        auth-events)))

