#|
    Copyright (C) 2019-2020 Gnuxie <Gnuxie@protonmail.com>
|#

;;; the only reason this is called matrix-room is because of cl:room.
(in-package #:architect.models.room-send)

(defclass matrix-room ()
  ((%state :initarg :state
           :accessor %state
           :type list ; alist
           :ghost t
           :json-ghost t
           :initform (list)
           :documentation "An alist representing the current state of the room.

Probably not the way to do this but it will do for now.")

   (%room-id :initarg :room-id
             :accessor room-id
             :key-name "room_id"
             :type string)

   (%previous-events :initarg :previous-events
                :accessor previous-events
                :initform (list)
                :type list
                :key-name "previous_events"
                :documentation "The most recent events, each making up
1/1 of the split.")

   (%depth :initarg :depth
           :accessor detph
           :initform 0
           :type fixnum
           :key-name "depth"
           :json-ghost t))
  (:metaclass json-schema.cl-mongo:json/collection-class))

(defgeneric authorize (room event)
  (:documentation "This is the authorization rules from
https://matrix.org/docs/spec/rooms/v1"))

(defgeneric reference-hash (room event)
  (:documentation
      "https://matrix.org/docs/spec/server_server/unstable#reference-hashes

needs to be specialized on room and also needs checking that it is unpadded

we aren't even sure this is going to be the format of room-v1 events either.
because they didn't write it in the spec.

"))

(defgeneric content-hash (room pdu)
  (:documentation "calculate the content hash of a pdu."))

(defgeneric base-pdu<-content (room &rest initargs)
  (:documentation "Takes the content part of an event turns it into a pdu

Base pdu is defined as a pdu without singing, event_id, unsinged any other rubbish."))

(defgeneric complete-pdu<-base-pdu (room base-pdu &key)
  (:documentation "Split into two steps so you can do the crypto-bs in here."))


(defgeneric add-from-content (room content &rest pdu-initargs)
    (:documentation "highest function to be used from the client-server api
event sending thingies"))

(defgeneric add-from-federated (room incoming-pdu &key)
  (:documentation "highest function to be used from the server-server api
for backfilling new events etc."))

(defgeneric update-room (room pdu &key)
  (:documentation "update the room model within the context of this new
complete pdu."))

(defgeneric store-new-pdu (room pdu)
  (:documentation ""))

(defgeneric mongo-doc<-room (room)
  (:documentation "save the room to cl-mongo

could easily make a metacl- why don't we just do-
hmm."))

(defgeneric getstate (room state-type &optional state-key)
  (:documentation "Get the state from the room."))
