#|
    Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:architect.models.room-send)

(defvar *room-worker-mailboxes*
  (make-array 16 :element-type 'safe-queue:mailbox :fill-pointer 0 :adjustable t
              :initial-element (safe-queue:make-mailbox))
  "if a thread dies, it's probably expected that the mailbox will be reused

This is sso that the buffered tasks do not get destroyed.")

(defmethod architect.hacks.async:partition-message ((topic (eql :room)) room-id)
  (declare (ignore topic))
  (architect.hacks.async:get-mailbox room-id *room-worker-mailboxes*))

(defun %create-room-worker (mailbox)
  "creates the worker function for the thread."
  (lambda ()
    (let ((*room-shelf* (make-room-shelf)))
      (architect.hacks.async:mailbox-executor mailbox))))

(defun add-room-worker ()
  (let ((mailbox (safe-queue:make-mailbox)))
    (bt:make-thread
     (%create-room-worker mailbox)
     :initial-bindings '((blackbird:*debug-on-error* . blackbird:*debug-on-error*)))
    (vector-push mailbox *room-worker-mailboxes*)))
