#|
    Copyright (C) 2019-2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:architect.models.room-send
  (:use #:cl)
  (:export
   #:*room-shelf*
   #:*room-shelves*
   #:*room-worker-mailboxes*
   #:make-room-shelf
   #:retrieve-room-from-store
   #:get-room
   #:add-room-worker
   #:get-mailbox

   ;; event authorization protocol
   #:matrix-room
   #:authorize
   #:reference-hash
   #:base-pdu<-content
   #:complete-pdu<-base-pdu
   #:add-from-content
   #:add-from-federated

   ;; room versions
   ;;; ¬ room v1
   #:room-v1
   ;;; ¬ mock-room
   #:mock-room
   #:room-id

   ))

(in-package #:architect.models.room-send)
