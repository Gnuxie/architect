#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:architect.models.room-send)

(defun preserve-keys (association &rest keys)
  (setf (cdr association)
        (remove-if-not (lambda (thing)
                         (member (car thing) keys :key #'car :test #'string=))
                       (cdr association))))

(defun redact-pdu (pdu)
  (let* ((alist-pdu (architect.hacks.canonical-json:alist<-json pdu))
         (content-association (assoc "content" alist-pdu :test #'string=)))
    (alexandria:switch ((cdr (assoc "type" alist-pdu)) :test #'string=)
      ("m.room.member" (preserve-keys content-association "membership"))
      ("m.room.create" (preserve-keys content-association "creator"))
      ("m.room.join_rules" (preserve-keys content-association "join_rule"))
      ("m.room.power_levels"
       (preserve-keys content-association
                      "ban" "events" "events_default" "kick" "redact" "state_default"
                      "users" "users_default"))
      ("m.room.aliases" (preserve-keys content-association "aliases"))
      ("m.room.history_visibility" (preserve-keys content-association
                                                  "history_visibility"))
      (t (setf (cdr content-association)
               nil)))
    (remove "unsigned" alist-pdu :test #'string= :key #'car)))
