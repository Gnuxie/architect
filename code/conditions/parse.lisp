#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:architect.conditions.generate)

(defun peek-next-char (stream)
  (peek-char nil stream nil nil t))

(defun read-condition (stream package)
  (cl-ppcre:register-groups-bind (name) ("``([\\S]+)``" (read-line stream))
    (let ((code (parse-integer (read-line stream)))
          (condition-name (intern (string-upcase name) package))
          (documentation
           (apply #'concatenate 'string
                  (loop :while (and (peek-next-char stream)
                                    (not (char= #\: (peek-next-char stream))))
                     :collect (read-line stream)))))
      `(progn
         (defpackage-plus-1:ensure-export '(,condition-name) ,package)
         (define-condition ,condition-name (api-error)
                ((%http-code :allocation :class :initform ,code)
                 (%error-code :allocation :class :initform ,name))
           (:documentation ,documentation))))))

(defun load-conditions (path package-name)
  (declare (type pathname path))
  (let ((package (progn (defpackage-plus-1:ensure-package package-name)
                        (defpackage-plus-1:inherit-package '#:architect.conditions.generate
                                                           package-name)
                        (find-package package-name))))
    (with-open-file (stream path :direction :input)
      (let ((conditions
             `(progn
                (defpackage-plus-1:ensure-package ,package-name)
                (defpackage-plus-1:inherit-package '#:architect.conditions.generate
                                                   ,package-name)
                ,@(loop :while (peek-next-char stream) :collect
                       (read-condition stream package)))))
        (let ((*package* package))
          (eval conditions))))))


