#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:architect.conditions.generate
  (:use #:cl)
  (:export
   ;; class
   #:api-error
   #:http-code
   #:error-code
   #:error-message
   #:internal-message
   ;; parse
   #:load-conditions
   #:read-condition
   ))

(in-package #:architect.conditions.generate)
