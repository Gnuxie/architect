#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:architect.conditions.generate)

(define-condition api-error (error)
  ((%http-code :allocation :class
               :reader http-code
               :initform 500
               :type fixnum
               :documentation "The http code that should be wrirten to the user.")

   (%error-code :allocation :class
                :reader error-code
                :initform "M_UNKNOWN"
                :type string
                :documentation "This is the errcode that is displayed in the json.")

   (%error-message :initarg :error-message
                   :reader error-message
                   :initform "An unknown error occurred."
                   :type string
                   :documentation "A error message to present to the user.")

   (%internal-message :initarg :internal-message
                      :reader internal-message
                      :type string
                      :documentation "A message to be used in logs/internally
that will not be presented to the user in the request response."))
  (:report (lambda (condition stream)
             (write-string (format nil "~&~a~%~a~%~%~a"
                                   (error-code condition)
                                   (internal-message condition)
                                   (error-message condition))
                           stream))))
