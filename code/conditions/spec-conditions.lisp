#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:architect.conditions.generate)

(load-conditions (asdf:system-relative-pathname :architect
                                                "code/conditions/client-server.txt")
                 :architect.conditions.client-server)
