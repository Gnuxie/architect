#|
    Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:architect.routes.client-server
  (:use #:cl #:architect.macros))

(in-package #:architect.routes.client-server)

(defun cs-point (resource &optional version)
  (when version (error "not implemented version handling yet"))
  (concatenate 'string "/_matrix/client/[^\/]+" resource))
