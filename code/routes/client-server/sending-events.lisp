#|
    Copyright (C) 2019-2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:architect.routes.client-server)

(wookie:defroute (:put (cs-point "/rooms/([^/]+)/send/([^/]+)/([^/]+)"))
    (request response arguments)
  (v:debug :fow "~a" (babel:octets-to-string (wookie:request-body request))
   )
  (with-param-binds (arguments room-id event-type txn-id)
    (with-json-content (request event architect.schema:m.room.message-content)
      (v:debug :wowf "~a" event)
      (architect.wookie:with-wookie-loop ((send-event room-id "@foo:hardcoded.net"
                                                      txn-id event)
                                          response result)
        (wookie:send-response response :body result
                              :headers '(:content-type "application/json"))))))

(defun %send-event (room-id sender txn-id content)
  (v:info :message-send "~&~w~%~w~%~w" room-id txn-id content)
  (let* ((room (architect.models.room-send:get-room room-id))
         (pdu 
          (architect.models.room-send:add-from-content
           room content :room-id room-id :sender sender)))
    (format nil "{\"event_id\" : ~s}" 
            (architect.schema:event-id pdu))))

(defun send-event (room-id sender txn-id event)
  (architect.hacks.async:defer-call (:room room-id)
      #'%send-event room-id sender txn-id event))




