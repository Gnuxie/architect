#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:architect.routes.client-server)

(wookie:defroute (:post (cs-point "/createRoom")) (request response)
  (architect.wookie:with-wookie-loop ((create-room "HARDCODED") response result)
    (wookie:send-response response :body result
                          :headers '(:content-type "application/json; charset=UTF-8"))))

(defun create-room (sender)
  (let* ((room-id (format nil "!(A)~a:~a" (get-universal-time)
                          "HARDCODED.NET")))
    (architect.hacks.async:defer-call (:room room-id)
        #'%create-room room-id sender)))


(defun %create-room (room-id sender)
  (declare (type string sender))
  (v:info :create-room "~&~w" sender)
  (let ((room
         (make-instance 'architect.models.room-send:mock-room
                        :room-id
                        room-id))

        (creation-event
         (make-instance 'architect.schema:m.room.create-content
                        :room-version "MOCK-V1"
                        :m.federate t
                        :creator sender))

        (sender-member-event
         (make-instance 'architect.schema:m.room.member-content
                        :membership "join"))

        (initial-power-levels
         (make-instance 'architect.schema:m.room.power-levels-content
                        :events-default 0
                        :state-default 50
                        :redact 50
                        :kick 50
                        :ban 50
                        :invite 0
                        :users
                        (jonathan:to-json
                         `((,sender . 1000))
                          :from :alist)
                        :events
                        (jonathan:to-json
                         `(("m.room.canonical_alias" . 50)
                           ("m.room.history_visibility" . 100)
                           ("m.room.avatar" . 50)
                           ("m.room.name" . 50)
                           ("m.room.power_levels" . 100))
                         :from :alist)))

        (join-rules
         (make-instance 'architect.schema:m.room.join-rules-content
                        :join-rule "invite"))
        (history-visibility
         (make-instance 'architect.schema:m.room.history-visibility-content
                        :history-visibility "shared")))
    

    ;; really need a transaction both on the mongo backend and a roomstate transaction.
    (cl-mongo-meta:db.insert room)
    (let ((to-authorize
           (list creation-event sender-member-event initial-power-levels join-rules history-visibility)))
      (mapc (lambda (event) ;; maybe we need something other than add-from-content just for state events?
              ;; maybe the easiest thing is to have another pdu for state events.
              ;; then all methods can be overriden.
              (architect.models.room-send:add-from-content room event :state-key ""))
            to-authorize))
    (format nil "{\"room_id\":\"~a\"}" (architect.models.room-send:room-id room))))
