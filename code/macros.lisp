#|
    Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:architect.macros
  (:use #:cl)
  (:export
   #:json-handler<-json-serializable-class
   #:with-json-content
   #:with-param-binds))

(in-package #:architect.macros)

(defgeneric json-handler<-json-serializable-class (class json-form)
  (:documentation "Create the code which parses the class into
a json serializable instance and validates the instance.

I guess we are expecting non local exists if we validation or parsing fails.")
  (:method ((class json-schema:json-serializable-class) json-form)
    (let ((instance (gensym)))
      `(let ((,instance
              (json-schema:make-instance-from-json ,class ,json-form)))
         ;; validate
         ,instance))))

(defmacro with-json-content ((request instance-var class-name) &body body)
  `(let ((,instance-var
         ,(json-handler<-json-serializable-class
          (find-class class-name)
          ;; probably want something better than this later on.
          `(babel:octets-to-string (wookie:request-body ,request)))))
     ,@body))

(defmacro with-param-binds ((arguments-var &rest symbols) &body body)
  `(symbol-macrolet ,(loop :for n :from 0
                        :for symbol :in symbols
                        :collect `(,symbol (nth ,n ,arguments-var)))
     ,@body))
