#|
    Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>
|#

;;; https://matrix.org/docs/spec/appendices#canonical-json
(defpackage #:architect.hacks.canonical-json
  (:use #:cl)
  (:export
   #:canonical-json
   #:alist<-json
))

(in-package #:architect.hacks.canonical-json)

(defgeneric alist<-json (object))
;;; can be improved by splitting the json-schema:jonathan protocol
;;; into predicate and value.
(defmethod alist<-json ((object json-schema:json-serializable))
  (let ((class (class-of object)))
    (loop :for ((effective-slotd slot))
       :on (reverse (json-schema.mop:slot-precedence-list class))
       :when (or (and (typep slot 'json-schema:json-serializable-slot)
                      (c2mop:slot-boundp-using-class class object effective-slotd))
                 (and (typep slot 'json-schema.cl-mongo:json/collection-slot)
                      (not (json-schema.cl-mongo::ghostp slot))
                      (c2mop:slot-boundp-using-class class object effective-slotd)))
       :collect (cons (json-schema:json-key-name slot)
                      (c2mop:slot-value-using-class class object effective-slotd)))))

(defun alistp (thing)
  "not very good.

Should really see how jonathan reads/encodes json into alists and determines if
the thing is an object, which is what we're actually checking for."
  (and (listp thing)
       (not (null (car (and (listp (car thing))
                            (car thing)))))
       (atom (car (car thing)))))

(defmethod alist<-json (object)
  (jonathan:parse object :as :alist))

(defun sort-alist (alist)
  (mapcar
   (lambda (thing)
     (if (alistp (cdr thing))
         (cons (car thing) (sort-alist (cdr thing)))
         thing))
   (sort alist #'string< :key #'car)))

(defgeneric canonical-json (object &rest keys-to-exclude)
  (:documentation "keys-to-exclude are keys to exclude in addition to keys
that would already be excluded in json-schema.")
  (:method (object &rest keys-to-exclude)
    (apply #'canonical-json (sort-alist (alist<-json object)) keys-to-exclude))
  (:method ((object json-schema:json-serializable) &rest keys-to-exclude)
    (let ((class (class-of object)))
      (jonathan:with-output-to-string*
        (json-schema.jonathan:with-object
          (loop :for ((effective-slotd slot))
             :on (json-schema.mop:slot-precedence-list class)
             :unless (member (json-schema:json-key-name slot)
                             keys-to-exclude
                             :test #'string=)
             :do (json-schema.jonathan:serialize-slot slot effective-slotd
                                                      class object))))))
  (:method ((sorted-alist list) &rest keys-to-exclude)
    (let ((clean-alist
           (remove-if (lambda (association)
                        (member (car association)
                                keys-to-exclude :test #'string=))
                      sorted-alist)))
      (jonathan:to-json clean-alist :from :alist))))


