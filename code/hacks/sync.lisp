#|
    Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:architect.hacks.sync
  (:use #:cl)
  (:export
   #:defer-task))

(in-package #:architect.hacks.sync)

(defun defer-task (target-mailbox fun &rest args)
  "defer task and await the result. This is blocking."
  (declare (type safe-queue:mailbox target-mailbox))
  (v:info :defer-task "~a ~a" fun args)
  (let ((reciever (safe-queue:make-mailbox)))
    (safe-queue:mailbox-send-message target-mailbox (list* reciever fun args))
    (safe-queue:mailbox-receive-message reciever)))
