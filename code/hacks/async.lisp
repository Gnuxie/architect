#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:architect.hacks.async
  (:use #:cl)
  (:export
   #:defer-task
   #:defer-call
   #:partition-message
   #:with-defered-call
   #:get-mailbox
   #:message
   #:message-function
   #:message-args
   #:message-resolver
   #:message-rejector
   #:execute-message
   #:mailbox-executor))

(in-package #:architect.hacks.async)

(defstruct message
  (function  (lambda ()) :type function :read-only t)
  (args nil :type list :read-only t)
  (resolver nil :type (or function null) :read-only t)
  (rejector nil :type (or function null) :read-only t))

(defun execute-message (message)
  "does not call the rejector, leaves that to the thread."
  (declare (type message message))
  (let ((result (apply (message-function message) (message-args message))))
    (when (message-resolver message)
      (funcall (message-resolver message) result))))

(defun defer-task (target-mailbox fun &rest args)
  (declare (type safe-queue:mailbox target-mailbox))
  (declare (type function fun))
  (bb:with-promise (resolve reject :resolve-fn resolver :reject-fn rejector)
    (safe-queue:mailbox-send-message
     target-mailbox (make-message :function fun :args args
                                  :resolver resolver :rejector rejector))))

(defmacro defer-call ((topic partition) function &rest args)
  `(defer-task (partition-message ,topic ,partition) ,function
     ,@args))

(defun get-mailbox (identifier place)
  (declare (optimize (speed 3) (safety 1)))
  (declare (type string identifier))
  (declare (type (vector safe-queue:mailbox) place))
  (aref place (mod (sxhash identifier) (length place))))

(defgeneric partition-message (topic partition)
  (:documentation "A topic is something like room-send
A parition is something like the room-id."))


(defmacro with-defered-call ((topic &optional partition) &body body)
  `(defer-task (partition-message ,topic ,partition)
      (lambda () ,@body)))
;;; so the plan is to have the request put into the clsoure that is the
;;; callback when we defer a task to the room wroker and that goes back
;;; into the webserver loop to write to the request

;;; then db io is in it's own event loop should we ever go async with that
;;; and that includes a rejector when given from a room server to rollback
;;; that event in the room should there be some kind of db error.
(defun mailbox-executor (mailbox)
  (tagbody
   :start
     (let ((message (safe-queue:mailbox-receive-message mailbox)))
       (tagbody
        :message-start
          (handler-bind
              ((error (lambda (c)
                        (unless blackbird:*debug-on-error*
                          (invoke-restart 'call-rejector c)))))
            (restart-case
                (execute-message message)
              (try-message-again ()
                (go :message-start))

              (drop-message ()
                (go :start))

              (call-rejector (condition)
                (v:debug :mailbox-executor "got condition~%~a" condition)
                (when (message-rejector message)
                  (funcall (message-rejector message)
                           condition))
                (go :start))))))
     (go :start)))

;; you'd use an event loop with an idler that gets the next message.
(defun create-response-worker (mailbox)
  (lambda ()
    (as:with-event-loop ()
      (mailbox-executor mailbox))))

#|

and then there's just gonna be missing room history 
hmm
idk if tha'ts allowed lol 
hmm
tbh this kind of db error would be fatal regardless of whether we knew there was a problem before federating 
but federating it means that the other servers would experiance the same fatal error if they were on the sample impl 
which is funny
the clients will be able to read them from the sync model before they're even persisted too 
not thought about how i'm going to split up the sync model yet 
it'd be cool if there was some kind of multi promise where you would wait until you got pieces from x threads 
you surly could make that 
basically a callback that adds to a closed over list and then the callback checks if the list is full, and if it is, it fulfills another promise
(which would be your high level multi promise)
so using this mechanism it would be possible to split the sync model by room too therefor actually eliminating the need for a sync model 
hmm
maybe not
it's probably a good idea to keep the reading model seperate to the writey model 
then another thing we can do is keep all sync requests in timing like a clock, so like every 10ms or something you recalculate the running sync model and the rest of the time you respond to requests about - hmm but then we have this problem where someone makes a sync request that's just slightly too old for the model and bajojsfjskfjk ok i wil shut up nwo
|#
