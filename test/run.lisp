#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:architect.test
  (:use #:cl)
  (:export
   #:architect.test
   #:run
   #:ci-run))

(in-package #:architect.test)

(parachute:define-test architect.test:architect.test)

(defun run (&key (report 'parachute:plain))
  (parachute:test 'architect.test:architect.test :report report))

(defun ci-run ()
  (setf (verbose:repl-level) :debug)
  (setf cl-mongo:*mongo-default-host* "testdb")
  (cl-mongo:db.use "test")
  (let ((test-result (run)))
    (when (not (null (parachute:results-with-status :failed test-result)))
      (uiop:quit -1))))
