#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:architect.test.routes.client-server
  (:use #:cl))

;;; we don't have a way to gracefully shutdown the server yet
;;; and we need one.
;;; might also be a good idea to chose which worker threads have debug enabled
;;; rather than just bdinging to bb:*debug-on-error*

(in-package #:architect.test.routes.client-server)

(defvar *started* nil)

(parachute:define-test client-server
  :parent (:architect.test :architect.test)

  (start))

(defun start ()
  (unless *started*
    (cl-mongo:db.use "test")
    (architect.models.room-send:add-room-worker)
    (setf (verbose:repl-level) :debug)
    (bt:make-thread
     (architect.wookie:make-server
      (make-instance 'wookie:listener :bind nil :port 4343)
      :wookie-debug t :blackbird-debug t))
    (setf *started* t)
    (sleep 1)))

(defvar *account* (cl-matrix:make-account "@foo:localhost" ""
                                          :origin "http://localhost:4343"))

(parachute:define-test create-room
  :parent client-server

  (parachute:of-type string (cl-matrix:room-create *account*)
                     "Failed to create default room."))
