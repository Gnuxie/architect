#|
    Copyright (C) 2019 Gnuxie <Gnuxie@protonmail.com>
|#
;;; this exists because of https://matrix.org/docs/spec/appendices#canonical-json
(defpackage #:architect.test.hacks
  (:use #:cl)
  (:export #:canonical-json))

(in-package #:architect.test.hacks)

(defvar *big-example*
 "{
    \"auth\": {
        \"success\": true,
        \"mxid\": \"@john.doe:example.com\",
        \"profile\": {
            \"display_name\": \"John Doe\",
            \"three_pids\": [
                {
                    \"medium\": \"email\",
                    \"address\": \"john.doe@example.org\"
                },
                {
                    \"medium\": \"msisdn\",
                    \"address\": \"123456789\"
                }
            ]
        }
    }
}" )

(parachute:define-test canonical-json
  :parent (:architect.test :architect.test)

  (parachute:is #'string=
                "{\"auth\":{\"mxid\":\"@john.doe:example.com\",\"profile\":{\"display_name\":\"John Doe\",\"three_pids\":[{\"address\":\"john.doe@example.org\",\"medium\":\"email\"},{\"address\":\"123456789\",\"medium\":\"msisdn\"}]},\"success\":true}}"
                (architect.hacks.canonical-json:canonical-json *big-example*)))
