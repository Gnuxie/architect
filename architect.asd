;;;; architect.asd

(asdf:defsystem #:architect
  :description "Partial open api parser, bootstrap for <yet to be named>

Ok no apparently it's all the same project now?"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "NON-VIOLENT PUBLIC LICENSE v4+"
  :version "0.0.1"
  :depends-on ("json-schema" "defpackage-plus" "cl-change-case"
                             "form-fiddle" "cl-cache-tables"
                             "safe-queue" "trivial-garbage" "verbose" "jonathan"
                             "json-schema.cl-mongo" "qbase64" "cl-mongo-meta"
                             "anaphora" "blackbird" "cl-async" "wookie")
  :serial t
  :components ((:module "code" :components
                        ((:file "macros")
                         (:module "schema-build" :components
                                  ((:file "json-schema-options")
                                   (:file "package")
                                   (:file "open-api")
                                   (:file "build")))
                         (:module "conditions" :components
                                  ((:file "package")
                                   (:file "class")
                                   (:file "parse")
                                   (:file "spec-conditions")))
                         (:module "hacks" :components
                                  ((:file "async")
                                   (:file "canonical-json")))
                         (:module "wookie" :components
                                  ((:file "loop")))
                         (:module "models" :components
                                  ((:module "room-send" :components
                                            ((:file "package")
                                             (:file "redaction")
                                             (:file "matrix-room")
                                             (:file "room-shelf")
                                             (:file "room-worker")
                                             (:file "mock-room")))))
                         (:module "routes" :components
                                  ((:module "client-server" :components
                                            ((:file "package")
                                             (:file "sending-events")
                                             (:file "room-create")))))))))


