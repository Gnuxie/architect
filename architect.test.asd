(asdf:defsystem #:architect.test
  :description "tests for architect"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "NON-VIOLENT PUBLIC LICENSE v4+"
  :version "0.0.1"
  :depends-on ("parachute" "architect" "cl-matrix")
  :serial t
  :components ((:module "test" :components
                        ((:file "run")
                         (:module "hacks" :components
                                  ((:file "canonical-json" )))
                         (:module "routes" :components
                                  ((:module "client-server" :components
                                            ((:file "room-create")))))))))
