# architect

[WIP] Matrix homeserver implementation in Common Lisp.

## update

project is being modularised and moved to https://gitlab.com/cal-coop/architect

## Aims

The aim of this project is as of writing fun, but it would be cool if architect could also: (but again idk how attainable these goals are, fun is foremost).

- Generate as much (helpful) code as possible while staying maintainable/extensible. 

- Compete with dendrite and other implementations.

- Be an example of Common Lisp's strengths and challenge the idea that you will pay for high-level abstraction in performance.

## Status

There's some really bad baggage while I buy time to chose what async webserver to use.
(See hacks/async)

I think the use of froutes is going to be dropped and the define-route macro
architect provides will just read the route provided and destructure it, this can't happen until another webserver is chosen though.

## Where to discuss architect?

You can find me in `#cl-matrix:matrix.org`, architect isn't important enough to have it's own room yet.


## License

NON-VIOLENT PUBLIC LICENSE V4+ see LICENSE.txt for details

